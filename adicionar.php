<?PHP
  require_once 'includes/database.php';
  require_once 'includes/errorhandler.php';
  require_once 'includes/filter.php';
  require_once 'includes/request.php';
  require_once 'includes/session.php';
  require_once 'includes/smarty.php';

  function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
    }
    return implode('', $pass); //turn the array into a string
  }

  if (isset($_SESSION['USER_ID']) && $_SESSION['USER_TYPE'] == 4) {
    if (isset(Request::array()[1]) && Request::array()[1] === 'aluno') {
      $num = Filter::get('numero');
      if (empty($num) || !preg_match('/^[a-zA-Z0-9]{1,16}$/', $num)) {
        $errors[] = '<b>Número de aluno</b> inválido!';
      }
      $name = Filter::get('nome');
      if (empty($name) || !preg_match('/^[a-zA-Z0-9\s]{1,32}$/', $name)) {
        $errors[] = '<b>Nome completo</b> inválido!';
      }
      $email = Filter::get('email');
      if (empty($email) || !preg_match('/^[a-zA-Z0-9@.-_]{1,64}$/', $email)) {
        $errors[] = '<b>Endereço de e-mail</b> inválido!';
      }

      if (!isset($errors)) {
        $pass = randomPassword();
        if(Database::query("INSERT INTO utilizador (login, password, nome, email, tipo) VALUES ('$num', PASSWORD('$pass'), '$name', '$email', '1');")) {
          require_once 'vendors/phpmailer/PHPMailer.php'; 
          require_once 'vendors/phpmailer/SMTP.php';
          require_once 'vendors/phpmailer/Exception.php';

          $mail = new PHPMailer\PHPMailer\PHPMailer();
          $mail->isSMTP();
          $mail->Host = 'smtp.mailtrap.io';
          $mail->SMTPAuth = true;
          $mail->Username = 'e0b9099ac36c77'; //paste one generated by Mailtrap
          $mail->Password = '1972d1bff2c184'; //paste one generated by Mailtrap
          $mail->SMTPSecure = 'tls';
          $mail->Port = 2525;
          $mail->setFrom('info@mailtrap.io', 'Mailtrap');
          $mail->addReplyTo('info@mailtrap.io', 'Mailtrap');
          $mail->addAddress("$email, $name");
          $mail->Subject = "Bem-vindo ao elboom";
          $mail->isHTML(true);
          $mail->Body = "Olá $name,<br /><br />A sua conta elboom foi criada com sucesso!<br />".
                        "<b>Use os seguintes dados para a aceder:</b><br />".
                        " -> Endereço de e-mail: $email<br /> -> Palavra-passe: $pass<br /><br />".
                        "Ficamos a espera da sua visita em <a href='aulas.luisbilhoto.pt/elboom/inicio'>elboom.pt</a>".
                        "<br /><br /><br />".
                        "Até breve, e bons estudos.<br />© ".date('Y')." ELBOOM";
          $mail->send();
        }
      }
      if (isset($errors)) { $smarty->assign('errors', $errors); }
      $smarty->display('adicionar_aluno.tpl');
    } else if (isset(Request::array()[1]) && Request::array()[1] === 'curso') {

      $smarty->display('adicionar_curso.tpl');
    } else if (isset(Request::array()[1]) && Request::array()[1] === 'disciplina') {

      $smarty->display('adicionar_disciplina.tpl');
    } else if (isset(Request::array()[1]) && Request::array()[1] === 'professor') {
      $num = Filter::get('numero');
      if (empty($num) || !preg_match('/^[a-zA-Z0-9]{1,16}$/', $num)) {
        $errors[] = '<b>Número de aluno</b> inválido!';
      }
      $name = Filter::get('nome');
      if (empty($name) || !preg_match('/^[a-zA-Z0-9\s]{1,32}$/', $name)) {
        $errors[] = '<b>Nome completo</b> inválido!';
      }
      $email = Filter::get('email');
      if (empty($email) || !preg_match('/^[a-zA-Z0-9@.-_]{1,64}$/', $email)) {
        $errors[] = '<b>Endereço de e-mail</b> inválido!';
      }

      if (!isset($errors)) {
        $pass = randomPassword();
        if(Database::query("INSERT INTO utilizador (login, password, nome, email, tipo) VALUES ('$num', PASSWORD('$pass'), '$name', '$email', '2');")) {
          require_once 'vendors/phpmailer/PHPMailer.php'; 
          require_once 'vendors/phpmailer/SMTP.php';
          require_once 'vendors/phpmailer/Exception.php';

          $mail = new PHPMailer\PHPMailer\PHPMailer();
          $mail->isSMTP();
          $mail->Host = 'smtp.mailtrap.io';
          $mail->SMTPAuth = true;
          $mail->Username = 'e0b9099ac36c77'; //paste one generated by Mailtrap
          $mail->Password = '1972d1bff2c184'; //paste one generated by Mailtrap
          $mail->SMTPSecure = 'tls';
          $mail->Port = 2525;
          $mail->setFrom('info@mailtrap.io', 'Mailtrap');
          $mail->addReplyTo('info@mailtrap.io', 'Mailtrap');
          $mail->addAddress("$email", "$name");
          $mail->Subject = "Bem-vindo ao elboom";
          $mail->isHTML(true);
          $mail->Body = "Olá $name,<br /><br />A sua conta elboom foi criada com sucesso!<br />".
                        "<b>Use os seguintes dados para a aceder:</b><br />".
                        " -> Endereço de e-mail: $email<br /> -> Palavra-passe: $pass<br /><br />".
                        "Ficamos a espera da sua visita em <a href='aulas.luisbilhoto.pt/elboom/inicio'>elboom.pt</a>".
                        "<br /><br /><br />".
                        "Até breve, e bom trabalho.<br />© ".date('Y')." ELBOOM";
          $mail->send();
        }
      }
      if (isset($errors)) { $smarty->assign('errors', $errors); }
      $smarty->display('adicionar_professor.tpl');
    } else {
      ErrorHandler::throw('404');
    }
  } else {
    ErrorHandler::throw('500');
  }
?>
