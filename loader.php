<?PHP
	require_once 'includes/request.php';

	$file = Request::array()[0] . '.php';
	if ($file == 'loader.php' || !file_exists($file)) {
		require 'includes/errorhandler.php';
		ErrorHandler::throw('404');
	} else {
		require $file;
	}

?>