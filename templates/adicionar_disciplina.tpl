{include file='components/header.tpl'}
<div class="app-content content pt-3">
  <div class="content-wrapper">
    <div class="content-body">
      <section class="users-edit">
        <div class="card">
          <div class="card-content">
            <div class="card-body">
              <div class="tab-content">
                <div
                  class="tab-pane active fade show"
                  id="utilizador"
                  aria-labelledby="utilizador-tab"
                  role="tabpanel"
                >
                  <!--form action="adicionar/curso" method="POST"-->
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                          <div class="controls">
                            <label>Número da disciplina</label>
                            <input
                              type="text"
                              class="form-control"
                              placeholder="Número da disciplina"
                              name="nome"
                              required
                            />
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <div class="controls">
                              <label>Nome da disciplina</label>
                              <input
                                type="text"
                                class="form-control"
                                placeholder="Nome da disciplina"
                                name="nome"
                                required
                              />
                            </div>
                          </div>
                      </div>
                      <div
                        class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1"
                      >
                        <button
                          type="submit"
                          class="btn btn-primary glow mb-1 mb-sm-0 mr-0 text-uppercase text-bold-500"
                          name="utilizador"
                        >
                          Guardar
                        </button>
                      </div>
                    </div>
                  <!--/form-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
{include file='components/footer.tpl'}
