{include file='components/header.tpl'}
        <div class="app-content content bg-white">
            <div class="content-overlay"></div>
            <div class="content-wrapper">
                <div class="content-header row"></div>
                <div class="content-body">
                    <section class="row flexbox-container">
                        <div class="col-xl-6 col-md-7 col-9">
                            <div class="card bg-transparent shadow-none">
                                <div class="card-content">
                                    <div class="card-body text-center bg-transparent miscellaneous">
{if isset($image) && $image != ''}
                                        <img class="img-fluid my-3" src="assets/images/errors/{$image}" alt="{$title}">
{/if}
                                        <h1 class="error-title mt-1 text-uppercase">{$error}</h1>
                                        <p class="p-2">{$message}</p>
                                        <a href="inicio" class="btn btn-primary round glow">VOLTAR AO INÍCIO</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
{include file='components/footer.tpl'}
