{include file='components/header.tpl'}
<div class="app-content content pt-3">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h4 class="content-header-title float-left pr-1 mb-0 border-0">Disciplina X</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="list-group-button-badge">
                <div class="row match-height">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Conteúdo</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <p>
                                        Materia disponivel durante o semestre.
                                    </p>
                                    <div class="list-group">
                                        <a href="#" class="list-group-item list-group-item-action active">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1 text-white">Apresentação</h5>
                                                <small>25 dias atrás</small>
                                            </div>
                                            <p class="mb-1">
                                                Introdução UC
                                            </p>
                                            <small>João Tristão</small>
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">Documentos de especificação</h5>
                                                <small>17 dias atrás</small>
                                            </div>
                                            <small>João Tristão</small>
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">Introdução à comunicação e documentação técnica</h5>
                                                <small>15 dias atrás</small>
                                            </div>
                                            <small>João Tristão</small>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
{include file='components/footer.tpl'}
