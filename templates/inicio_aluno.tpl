{include file='components/header.tpl'}
    <div class="app-content content pt-3">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h4 class="content-header-title float-left pr-1 mb-0 border-0">
                  Minhas Disciplinas
                </h4>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="content-body text-center">
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Arquitetura Tecnológica de Sistemas de Informação</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Big Data</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Documentação Técnica</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Engenharia de Software</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="content-body text-center">
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Ferramentas de Teste de Software</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Programação Web com Bases de Dados</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Programação para Dispositivos Móveis</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
              <section class="card overflow-hidden mb-1">
                <a href="disciplina">
                  <div class="card-header">
                    <h4 class="card-title">Segurança de Sistemas Informáticos</h4>
                    <small>1º Semestre</small>
                  </div>
                </a>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
{include file='components/footer.tpl'}
