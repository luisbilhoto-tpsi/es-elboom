{include file='components/header.tpl'}
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="auth-login" class="row flexbox-container">
            <div class="col-xl-8 col-11">
              <div class="col-lg-6 col-md-8 col-sm-10 col-12 pb-3 m-auto pt-1">
                <div class="card-content">
                  <img class="img-fluid" src="assets/images/logo.png" alt="elboom Logotípo" />
                </div>
              </div>
{if isset($errors)}
              <div class="alert alert-danger alert-dismissible col-lg-6 col-md-8 col-sm-10 col-12 ml-auto mb-2 mr-auto p-1" role="alert">
                <h4 class="alert-heading d-flex font-weight-bold">
                  <i class="bx bx-error" style="font-size: 26px;"></i> ERRO AO ENTRAR!
                </h4>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button><hr class="mt-0"/>
                <div class="align-items-center">
                  <ul class="pl-1 mb-0">
{section name=i loop=$errors}
                    <li>{$errors[i]}</li>
{/section}
                  </ul>
                </div>
              </div>
{/if}
              <div class="col-lg-6 col-md-8 col-sm-10 col-12 px-0 m-auto">
                <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                  <div class="card-header pb-1">
                    <div class="card-title">
                      <h3 class="text-center text-bold-700 mb-2">BEM-VINDO</h3>
                    </div>
                  </div>
                  <div class="card-content">
                    <div class="card-body">
                      <form action="inicio" method="POST">
                        <div class="form-group mb-50">
                          <label class="text-bold-600" for="email">Número ou e-mail</label>
                          <input type="text" class="form-control" id="user" name="user" placeholder="Número ou e-mail" required />
                        </div>
                        <div class="form-group">
                          <label class="text-bold-600" for="password">Palavra-passe</label>
                          <input type="password" class="form-control" id="password" name="password" placeholder="Palavra-passe" required />
                        </div>
                        <div class="form-group d-flex flex-sm-row flex-column justify-content-between align-items-center">
                          <div class="text-left">
                            <div class="checkbox checkbox-sm">
                              <input type="checkbox" class="form-check-input" id="remember" name="remember" />
                              <label class="checkboxsmall" for="remember"><small>Lembrar-me</small></label>
                            </div>
                          </div>
                          <div class="text-right">
                            <a href="#" class="card-link"><small>Esqueceu-se da palavra-passe?</small></a>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-primary glow w-100 position-relative text-bold-500" id="submit" name="submit">
                          ENTRAR <i id="icon-arrow" class="bx bx-right-arrow-alt float-right"></i>
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
{include file='components/footer.tpl'}
