{include file='components/header.tpl'}
<!-- BEGIN: Content-->
<div class="app-content content pt-3">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users list start -->
            <section class="users-list-wrapper">
                <div class="users-list-filter px-1 bg-white">
                    <form>
                        <div class="row border rounded py-2 mb-2">
                            <div class="col-12">
                                <h4 class="content-header-title float-left pr-1 mb-0 border-0">Faltas</h4>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <label for="users-list-verified">Semestre</label>
                                <fieldset class="form-group">
                                    <select class="form-control" id="users-list-verified">
                                            <option value="">Sem filtro</option>
                                            <option value="Yes">1</option>
                                            <option value="No">2</option>
                                        </select>
                                </fieldset>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <label for="users-list-status">Estado</label>
                                <fieldset class="form-group">
                                    <select class="form-control" id="users-list-status">
                                            <option value="">Sem filtro</option>
                                            <option value="Ativo">Ativo</option>
                                            <option value="Terminado">Terminado</option>
                                            <option value="Outros">Outros</option>
                                        </select>
                                </fieldset>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                <button type="reset" class="btn btn-primary btn-block glow users-list-clear mb-0">Procurar</button>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                <button type="reset" class="btn btn-light-secondary btn-block glow users-list-clear mb-0">Limpar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="users-list-table">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <!-- datatable start -->
                                <div class="table-responsive">
                                    <table id="users-list-datatable" class="table">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Email</th>
                                                <th>Nome</th>
                                                <th>Ultima sessão</th>
                                                <th>Estado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>300</td>
                                                <td><a href="#">dean3004@elboom.pt</a>
                                                </td>
                                                <td>Dean Stanley</td>
                                                <td>30/04/2019</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>301</td>
                                                <td><a href="#">zena0604@elboom.pt</a>
                                                </td>
                                                <td>Zena Buckley</td>
                                                <td>06/04/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>302</td>
                                                <td><a href="#">delilah0301@elboom.pt</a>
                                                </td>
                                                <td>Delilah Moon</td>
                                                <td>03/01/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>303</td>
                                                <td><a href="#">hillary1807@elboom.pt</a>
                                                </td>
                                                <td>Hillary Rasmussen</td>
                                                <td>18/07/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>304</td>
                                                <td><a href="#">herman2003@elboom.pt</a>
                                                </td>
                                                <td>Herman Tate</td>
                                                <td>20/03/2020</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>305</td>
                                                <td><a href="#">kuame3008@elboom.pt</a>
                                                </td>
                                                <td>Kuame Ford</td>
                                                <td>30/08/2019</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>306</td>
                                                <td><a href="#">fulton2009@elboom.pt</a>
                                                </td>
                                                <td>Fulton Stafford</td>
                                                <td>20/09/2019</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>307</td>
                                                <td><a href="#">piper0508@elboom.pt</a>
                                                </td>
                                                <td>Piper Jordan</td>
                                                <td>05/08/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>308</td>
                                                <td><a href="#">neil1002@elboom.pt</a>
                                                </td>
                                                <td>Neil Sosa</td>
                                                <td>10/02/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>309</td>
                                                <td><a href="#">caldwell2402@elboom.pt</a>
                                                </td>
                                                <td>Caldwell Chapman</td>
                                                <td>24/02/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>310</td>
                                                <td><a href="#">wesley0508@elboom.pt</a>
                                                </td>
                                                <td>Wesley Oneil</td>
                                                <td>05/08/2020</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>311</td>
                                                <td><a href="#">tallulah2009@elboom.pt</a>
                                                </td>
                                                <td>Tallulah Fleming</td>
                                                <td>20/09/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>312</td>
                                                <td><a href="#">iris2505@elboom.pt</a>
                                                </td>
                                                <td>Iris Maddox</td>
                                                <td>25/05/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>313</td>
                                                <td><a href="#">caleb1504@elboom.pt</a>
                                                </td>
                                                <td>Caleb Bradley</td>
                                                <td>15/04/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>314</td>
                                                <td><a href="#">illiana0410@elboom.pt</a>
                                                </td>
                                                <td>Illiana Grimes</td>
                                                <td>04/10/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>315</td>
                                                <td><a href="#">chester0902@elboom.pt</a>
                                                </td>
                                                <td>Chester Estes</td>
                                                <td>09/02/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>316</td>
                                                <td><a href="#">gregory2309@elboom.pt</a>
                                                </td>
                                                <td>Gregory Hayden</td>
                                                <td>23/09/2019</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>317</td>
                                                <td><a href="#">jescie1802@elboom.pt</a>
                                                </td>
                                                <td>Jescie Parker</td>
                                                <td>18/02/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>318</td>
                                                <td><a href="#">sydney3101@elboom.pt</a>
                                                </td>
                                                <td>Sydney Cabrera</td>
                                                <td>31/01/2020</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>319</td>
                                                <td><a href="#">gray2702@elboom.pt</a>
                                                </td>
                                                <td>Gray Valenzuela</td>
                                                <td>27/02/2020</td>
                                                <td><span class="badge badge-light-warning">Atrasado</span></td>
                                            </tr>
                                            <tr>
                                                <td>320</td>
                                                <td><a href="#">hoyt0305@elboom.pt</a>
                                                </td>
                                                <td>Hoyt Ellison</td>
                                                <td>03/05/2020</td>
                                                <td><span class="badge badge-light-success">Presente</span></td>
                                            </tr>
                                            <tr>
                                                <td>321</td>
                                                <td><a href="#">damon0209@elboom.pt</a>
                                                </td>
                                                <td>Damon Berry</td>
                                                <td>02/09/2019</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>322</td>
                                                <td><a href="#">kelsie0511@elboom.pt</a>
                                                </td>
                                                <td>Kelsie Dunlap</td>
                                                <td>05/11/2019</td>
                                                <td><span class="badge badge-light-warning">Atrasado</span></td>
                                            </tr>
                                            <tr>
                                                <td>323</td>
                                                <td><a href="#">abel1606@elboom.pt</a>
                                                </td>
                                                <td>Abel Dunn</td>
                                                <td>16/06/2020</td>
                                                <td><span class="badge badge-light-danger">Faltou</span></td>
                                            </tr>
                                            <tr>
                                                <td>324</td>
                                                <td><a href="#">nina2208@elboom.pt</a>
                                                </td>
                                                <td>Nina Byers</td>
                                                <td>22/08/2019</td>
                                                <td><span class="badge badge-light-warning">Atrasado</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- datatable ends -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- users list ends -->
        </div>
    </div>
</div>
<!-- END: Content-->
{include file='components/footer.tpl'}
