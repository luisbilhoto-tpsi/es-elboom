{include file='components/header.tpl'}
    <div class="app-content content pt-3">
      <div class="content-wrapper">
        <div class="content-body">
          <section class="users-edit">
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <ul class="nav nav-tabs mb-2" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link d-flex align-items-center active" id="utilizador-tab" data-toggle="tab" href="#utilizador" aria-controls="utilizador" role="tab" aria-selected="true">
                        <i class="bx bx-user mr-25"></i><span class="d-none d-sm-block">Utilizador</span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link d-flex align-items-center" id="seguranca-tab" data-toggle="tab" href="#seguranca" aria-controls="seguranca" role="tab" aria-selected="false">
                        <i class="bx bx-info-circle mr-25"></i><span class="d-none d-sm-block">Segurança</span>
                      </a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active fade show" id="utilizador" aria-labelledby="utilizador-tab" role="tabpanel">
                      <form action="utilizador/editar" method="POST" enctype="multipart/form-data">
                        <div class="media mb-2">
                          <img src="uploads/avatars/{$smarty.session.USER_AVATAR}" alt="{$smarty.session.USER_NAME}" class="users-avatar-shadow rounded-circle" height="64" width="64">
                          <div class="media-body ml-2">
                            <h4 class="media-heading">Imagem de Utilizador</h4>
                            <div class="col-12 px-0 d-flex text-uppercase">
                              <label for="avatar" class="btn btn-sm btn-primary mr-25 mb-0 cursor-pointer">
                                Mudar
                              </label>
                              <input type="file" class="d-none" id="avatar" name="avatar">
                              <a href="utilizador/editar?action=avatar_reset" class="btn btn-sm btn-light-secondary">
                                Apagar
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12 col-sm-6">
                            <div class="form-group">
                              <div class="controls">
                                <label>Nome completo</label>
                                <input type="text" class="form-control" placeholder="Nome completo" name="nome" value="{$user.nome}" required />
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6">
                            <div class="form-group">
                              <div class="controls">
                                <label>Endereço de e-mail</label>
                                <input type="email" class="form-control" placeholder="Endereço de e-mail" name="email" value="{$user.email}" required />
                              </div>
                            </div>
                          </div>
                          <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                            <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 text-uppercase text-bold-500" name="utilizador">
                              Guardar
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane fade show" id="seguranca" aria-labelledby="seguranca-tab" role="tabpanel">
                      <form action="utilizador/editar" method="POST">
                        <div class="row">
                          <div class="col-12 col-sm-6">
                            <h5 class="mb-1"><i class="bx bx-link mr-25"></i>Alterar Palavra-Passe</h5>
                            <div class="form-group">
                              <label>Nova Palavra-Passe</label>
                              <input class="form-control" type="password" name="password1">
                            </div>
                            <div class="form-group">
                              <label>Confirmar Palavra-Passe</label>
                              <input class="form-control" type="password" name="password2">
                            </div>
                          </div>
                          <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                            <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 text-uppercase text-bold-500" name="seguranca">
                              Guardar
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
{include file='components/footer.tpl'}
