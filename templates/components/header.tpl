<!DOCTYPE html>
<html class="loading" lang="pt">
  <head>
    <base href="{$base_url}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>{if isset($page_title)}{$page_title} - {/if}elboom</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/styles/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/colors.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/components.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/themes/semi-dark-layout.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/pages/app-chat.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/pages/authentication.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/pages/page-users.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/style.css">
  </head>
{if isset($fullscreen) && $fullscreen}
  <body class="vertical-layout vertical-menu-modern 1-column navbar-sticky footer-static bg-full-screen-image blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
{else}
  <body class="vertical-layout vertical-menu-modern 2-columns{if isset($application)} {$application}-application {/if}navbar-sticky footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="navbar-collapse" id="navbar-mobile">
            <!-- TODO: This should be removed -->
            <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
              <ul class="nav navbar-nav bookmark-icons">
                <li class="nav-item d-block d-xl-none">
                  <a class="nav-link navbar-brand mr-1 pr-0 pl-0" href="inicio">
                    <div class="brand-logo">
                      <img class="logo d-md-none" src="assets/images/logo-mobile.png" height="26px"/>
                      <img class="logo d-none d-md-block" src="assets/images/logo.png" height="26px"/>
                    </div>
                  </a>
                </li>
                <li class="nav-item d-block d-xl-none">
                  <a class="nav-link navbar-brand mr-1 pr-0 pl-0" href="inicio" data-toggle="tooltip" data-placement="top" title="Disciplinas">
                    <i class="ficon bx bxs-bank"></i>
                  </a>
                </li>
                <li class="nav-item d-block d-xl-none">
                  <a class="nav-link navbar-brand mr-1 pr-0 pl-0" href="#" data-toggle="tooltip" data-placement="top" title="Calendário">
                    <i class="ficon bx bxs-calendar"></i>
                  </a>
                </li>
                <li class="nav-item d-block d-xl-none">
                  <a class="nav-link navbar-brand mr-1 pr-0 pl-0" href="faltas" data-toggle="tooltip" data-placement="top" title="Faltas">
                    <i class="ficon bx bxs-check-circle"></i>
                  </a>
                </li>
              </ul>
            </div>
            <!-- END -->
            <ul class="nav navbar-nav float-right">
{if $smarty.session.USER_TYPE == 4}
              <li class="dropdown dropdown-language nav-item">
                <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ficon bx bx-plus"></i> <span class="selected-language">Adicionar</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                  <a class="dropdown-item" href="adicionar/aluno">Aluno</a>
                  <a class="dropdown-item" href="adicionar/professor">Professor</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="adicionar/curso">Curso</a>
                  <a class="dropdown-item" href="adicionar/disciplina">Disciplina</a>
                  <a class="dropdown-item" href="adicionar/turma">Turma</a>
                </div>
              </li>
{/if}
              <li class="nav-item nav-search">
                <a class="nav-link nav-link-search">
                  <i class="ficon bx bx-search"></i>
                </a>
                <div class="search-input">
                  <div class="search-input-icon">
                    <i class="bx bx-search primary"></i>
                  </div>
                  <input class="input" type="text" placeholder="Procurar.." tabindex="-1" data-search="template-search" />
                  <div class="search-input-close"><i class="bx bx-x"></i></div>
                  <ul class="search-list"></ul>
                </div>
              </li>
              <li class="dropdown dropdown-notification nav-item">
                <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                  <i class="ficon bx bx-bell bx-flip-horizontal"></i>
                  <span class="badge badge-pill badge-danger badge-up">0</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <div class="dropdown-header px-1 py-75 d-flex justify-content-between">
                      <span class="notification-title">Notificações</span>
                      <span class="text-bold-400 cursor-pointer">Marcar todas como lidas</span>
                    </div>
                  </li>
                  <li class="scrollable-container media-list">
                    <div class="text-center p-2">
                      Não existem novas notificações
                    </div>
                  </li>
                </ul>
              </li>
              <li class="dropdown dropdown-user nav-item">
                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <div class="user-nav d-sm-flex d-none">
                    <span class="user-name">{$smarty.session.USER_NAME}</span>
                    <span class="user-status text-muted">{$smarty.session.USER_TNOME}</span>
                  </div>
                  <img class="round" src="uploads/avatars/{$smarty.session.USER_AVATAR}" alt="{$smarty.session.USER_NAME}" height="40" width="40">
                </a>
                <div class="dropdown-menu dropdown-menu-right pb-0">
                  <a class="dropdown-item" href="utilizador/editar">
                    <i class="bx bx-user mr-50"></i> Editar Perfil
                  </a>
                  <div class="dropdown-divider mb-0"></div>
                  <a class="dropdown-item" href="inicio?action=sair">
                    <i class="bx bx-power-off mr-50"></i> Sair
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- Main menu -->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mr-auto">
            <a class="navbar-brand" href="inicio">
              <div class="brand-logo"><img class="logo" src="assets/images/logo.png" /></div>
            </a>
          </li>
        </ul>
      </div>
      <div class="shadow-bottom"></div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
          <li id="open" class="nav-item">
            <a href="inicio">
              <i class="menu-livicon" data-icon="bank"></i>
              <span class="menu-title" data-i18n="Disciplinas">Disciplinas</span>
              <span class="badge badge-light-danger badge-pill badge-round float-right mr-2">8</span>
            </a>
            <ul class="menu-content">
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Arquitetura Tecnol..</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Big Data</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Documentação Téc..</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Engenharia de Soft..</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Ferramentas de Teste de Software</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Programação Web ..</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Programação para ..</span>
                </a>
              </li>
              <li>
                <a href="disciplina">
                  <i class="bx bx-right-arrow-alt"></i>
                  <span class="menu-item" data-i18n="Disciplina">Segurança de Sistemas Informáticos</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#">
              <i class="menu-livicon" data-icon="calendar"></i>
              <span class="menu-title" data-i18n="Calendário">Calendário</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="faltas">
              <i class="menu-livicon" data-icon="check-alt"></i>
              <span class="menu-title" data-i18n="Faltas">Faltas</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <script>
      window.onload = function() {
        document.getElementById('open').classList.add("open");
      }
    </script>
{/if}
