{if !isset($fullscreen) || !$fullscreen}
    <footer class="footer footer-static footer-light">
      <p class="clearfix mb-0">
        <span class="float-left d-inline-block"><b>&copy; ELBOOM {$smarty.now|date_format:"Y"}</b></span>
        <span class="float-right d-sm-inline-block d-none">
          <a class="font-weight-bold" href="#">Política de Privacidade</a> | 
          <a class="font-weight-bold" href="#">Mapa do Site</a>
        </span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
      </p>
    </footer>
{/if}

    <script type="text/javascript" src="assets/scripts/vendors.min.js"></script>
    <script type="text/javascript" src="assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script type="text/javascript" src="assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script type="text/javascript" src="assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <script type="text/javascript" src="assets/scripts/scripts/configs/vertical-menu-light.js"></script>
    <script type="text/javascript" src="assets/scripts/core/app-menu.js"></script>
    <script type="text/javascript" src="assets/scripts/core/app.js"></script>
    <script type="text/javascript" src="assets/scripts/scripts/components.js"></script>
    <script type="text/javascript" src="assets/scripts/scripts/footer.js"></script>
    <script type="text/javascript" src="assets/scripts/scripts/pages/page-users.js"></script>
    <script type="text/javascript" src="assets/scripts/scripts/navs/navs.js"></script>
  </body>
</html>
