/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : elboom

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 20/01/2021 16:22:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assiduidade
-- ----------------------------
DROP TABLE IF EXISTS `assiduidade`;
CREATE TABLE `assiduidade`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pagina` bigint(20) UNSIGNED NOT NULL,
  `inicio` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fim` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_assiduidade_pagina`(`pagina`) USING BTREE,
  CONSTRAINT `fk_assiduidade_pagina` FOREIGN KEY (`pagina`) REFERENCES `pagina` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assiduidade
-- ----------------------------

-- ----------------------------
-- Table structure for assiduidade_utilizador
-- ----------------------------
DROP TABLE IF EXISTS `assiduidade_utilizador`;
CREATE TABLE `assiduidade_utilizador`  (
  `assiduidade` bigint(20) UNSIGNED NOT NULL,
  `utilizador` bigint(20) UNSIGNED NOT NULL,
  `marcada` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`assiduidade`, `utilizador`, `marcada`) USING BTREE,
  INDEX `fk_assiduidade_utilizador_utilizador`(`utilizador`) USING BTREE,
  CONSTRAINT `fk_assiduidade_utilizador_assiduidade` FOREIGN KEY (`assiduidade`) REFERENCES `assiduidade` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_assiduidade_utilizador_utilizador` FOREIGN KEY (`utilizador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assiduidade_utilizador
-- ----------------------------

-- ----------------------------
-- Table structure for conteudo
-- ----------------------------
DROP TABLE IF EXISTS `conteudo`;
CREATE TABLE `conteudo`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `descricao` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `enviado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pagina` bigint(20) UNSIGNED NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`, `pagina`) USING BTREE,
  INDEX `fk_conteudo_pagina`(`pagina`) USING BTREE,
  INDEX `fk_conteudo_tipo`(`tipo`) USING BTREE,
  CONSTRAINT `fk_conteudo_pagina` FOREIGN KEY (`pagina`) REFERENCES `pagina` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_conteudo_tipo` FOREIGN KEY (`tipo`) REFERENCES `conteudo_tipo` (`tipo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of conteudo
-- ----------------------------

-- ----------------------------
-- Table structure for conteudo_tipo
-- ----------------------------
DROP TABLE IF EXISTS `conteudo_tipo`;
CREATE TABLE `conteudo_tipo`  (
  `tipo` tinyint(1) NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tipo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of conteudo_tipo
-- ----------------------------
INSERT INTO `conteudo_tipo` VALUES (0, 'Mensagem');
INSERT INTO `conteudo_tipo` VALUES (1, 'Download');

-- ----------------------------
-- Table structure for curso
-- ----------------------------
DROP TABLE IF EXISTS `curso`;
CREATE TABLE `curso`  (
  `codigo` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `coordenador` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `fk_curso_coordenador`(`coordenador`) USING BTREE,
  CONSTRAINT `fk_curso_coordenador` FOREIGN KEY (`coordenador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of curso
-- ----------------------------

-- ----------------------------
-- Table structure for curso_disciplina
-- ----------------------------
DROP TABLE IF EXISTS `curso_disciplina`;
CREATE TABLE `curso_disciplina`  (
  `curso` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `disciplina` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `coordenador` bigint(20) UNSIGNED NOT NULL,
  `ano` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`curso`, `disciplina`) USING BTREE,
  INDEX `fk_curso_disciplina_disciplina`(`disciplina`) USING BTREE,
  INDEX `fk_curso_disciplina_coordenador`(`coordenador`) USING BTREE,
  CONSTRAINT `fk_curso_disciplina_coordenador` FOREIGN KEY (`coordenador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_curso_disciplina_curso` FOREIGN KEY (`curso`) REFERENCES `curso` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_curso_disciplina_disciplina` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of curso_disciplina
-- ----------------------------

-- ----------------------------
-- Table structure for disciplina
-- ----------------------------
DROP TABLE IF EXISTS `disciplina`;
CREATE TABLE `disciplina`  (
  `codigo` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of disciplina
-- ----------------------------

-- ----------------------------
-- Table structure for horario
-- ----------------------------
DROP TABLE IF EXISTS `horario`;
CREATE TABLE `horario`  (
  `turma` bigint(20) UNSIGNED NOT NULL,
  `pagina` bigint(20) UNSIGNED NOT NULL,
  `sala` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `hora` time NOT NULL DEFAULT '00:00:00',
  `dia` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`turma`, `pagina`) USING BTREE,
  INDEX `fk_horario_pagina`(`pagina`) USING BTREE,
  INDEX `fk_horario_dia`(`dia`) USING BTREE,
  CONSTRAINT `fk_horario_dia` FOREIGN KEY (`dia`) REFERENCES `horario_dia` (`dia`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_horario_pagina` FOREIGN KEY (`pagina`) REFERENCES `pagina` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_horario_turma` FOREIGN KEY (`turma`) REFERENCES `turma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of horario
-- ----------------------------

-- ----------------------------
-- Table structure for horario_dia
-- ----------------------------
DROP TABLE IF EXISTS `horario_dia`;
CREATE TABLE `horario_dia`  (
  `dia` tinyint(1) UNSIGNED NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`dia`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of horario_dia
-- ----------------------------
INSERT INTO `horario_dia` VALUES (1, 'Domingo');
INSERT INTO `horario_dia` VALUES (2, 'Segunda-feira');
INSERT INTO `horario_dia` VALUES (3, 'Terça-feira');
INSERT INTO `horario_dia` VALUES (4, 'Quarta-feira');
INSERT INTO `horario_dia` VALUES (5, 'Quinta-feira');
INSERT INTO `horario_dia` VALUES (6, 'Sexta-feira');
INSERT INTO `horario_dia` VALUES (7, 'Sábado');

-- ----------------------------
-- Table structure for mensagem
-- ----------------------------
DROP TABLE IF EXISTS `mensagem`;
CREATE TABLE `mensagem`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `conteudo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `utilizador` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_mensagem_utilizador`(`utilizador`) USING BTREE,
  CONSTRAINT `fk_mensagem_utilizador` FOREIGN KEY (`utilizador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mensagem
-- ----------------------------

-- ----------------------------
-- Table structure for mensagem_utilizador
-- ----------------------------
DROP TABLE IF EXISTS `mensagem_utilizador`;
CREATE TABLE `mensagem_utilizador`  (
  `mensagem` bigint(20) UNSIGNED NOT NULL,
  `utilizador` bigint(20) UNSIGNED NOT NULL,
  `vista` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`mensagem`, `utilizador`) USING BTREE,
  INDEX `fk_mensagem_utilizador_utilizador`(`utilizador`) USING BTREE,
  CONSTRAINT `fk_mensagem_utilizador_mensagem` FOREIGN KEY (`mensagem`) REFERENCES `mensagem` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_mensagem_utilizador_utilizador` FOREIGN KEY (`utilizador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mensagem_utilizador
-- ----------------------------

-- ----------------------------
-- Table structure for pagina
-- ----------------------------
DROP TABLE IF EXISTS `pagina`;
CREATE TABLE `pagina`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `descricao` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `disciplina` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `turma` bigint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_pagina_disciplina`(`disciplina`) USING BTREE,
  INDEX `fk_pagina_turma`(`turma`) USING BTREE,
  CONSTRAINT `fk_pagina_disciplina` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_pagina_turma` FOREIGN KEY (`turma`) REFERENCES `turma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pagina
-- ----------------------------

-- ----------------------------
-- Table structure for pagina_utilizador
-- ----------------------------
DROP TABLE IF EXISTS `pagina_utilizador`;
CREATE TABLE `pagina_utilizador`  (
  `pagina` bigint(20) UNSIGNED NOT NULL,
  `docente` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`pagina`, `docente`) USING BTREE,
  INDEX `fk_pagina_utilizador_docente`(`docente`) USING BTREE,
  CONSTRAINT `fk_pagina_utilizador_docente` FOREIGN KEY (`docente`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_pagina_utilizador_pagina` FOREIGN KEY (`pagina`) REFERENCES `pagina` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pagina_utilizador
-- ----------------------------

-- ----------------------------
-- Table structure for turma
-- ----------------------------
DROP TABLE IF EXISTS `turma`;
CREATE TABLE `turma`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'A',
  `coordenador` bigint(255) UNSIGNED NOT NULL,
  `curso` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `ano` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_turma_coordenador`(`coordenador`) USING BTREE,
  CONSTRAINT `fk_turma_coordenador` FOREIGN KEY (`coordenador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of turma
-- ----------------------------

-- ----------------------------
-- Table structure for utilizador
-- ----------------------------
DROP TABLE IF EXISTS `utilizador`;
CREATE TABLE `utilizador`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `imagem` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'default.svg',
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_utilizador_tipo`(`tipo`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  CONSTRAINT `fk_utilizador_tipo` FOREIGN KEY (`tipo`) REFERENCES `utilizador_tipo` (`tipo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of utilizador
-- ----------------------------
INSERT INTO `utilizador` VALUES (1, '', '*7C0967E7025B4584DDE9673A339039933D937F49', 'Desativado', 'desativado@elboom.pt', 'default.svg', '2021-01-20 12:20:26', 0);
INSERT INTO `utilizador` VALUES (2, '', '*CA60DEC1721F1E71CC333B5D93B0AA263C3BCC6C', 'Aluno 1', 'aluno1@elboom.pt', 'default.svg', '2021-01-20 12:20:26', 1);
INSERT INTO `utilizador` VALUES (3, '', '*BD703AA16F05BD18720A16263E1CE3DB4B01CF86', 'Aluno 2', 'aluno2@elboom.pt', 'default.svg', '2021-01-20 12:20:26', 1);
INSERT INTO `utilizador` VALUES (4, '', '*4C45692E20F0A7D42D3C736F45E7AB3EB3A9D070', 'Professor', 'professor@elboom.pt', 'default.svg', '2021-01-20 12:20:26', 2);
INSERT INTO `utilizador` VALUES (5, '', '*E884FEA6E8373404AAC1CBFA030F9AB10518B83C', 'Coordenador', 'coordenador@elboom.pt', 'default.svg', '2021-01-20 12:20:26', 3);
INSERT INTO `utilizador` VALUES (6, '', '*EFC258DA67C9F942793CDD8A2050C469256C7192', 'Administrador', 'administrador@elboom.pt', 'default.svg', '2021-01-20 12:20:26', 4);

-- ----------------------------
-- Table structure for utilizador_pagina
-- ----------------------------
DROP TABLE IF EXISTS `utilizador_pagina`;
CREATE TABLE `utilizador_pagina`  (
  `utilizador` bigint(20) UNSIGNED NOT NULL,
  `pagina` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`utilizador`, `pagina`) USING BTREE,
  INDEX `fk_utilizador_pagina_pagina`(`pagina`) USING BTREE,
  CONSTRAINT `fk_utilizador_pagina_pagina` FOREIGN KEY (`pagina`) REFERENCES `pagina` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_utilizador_pagina_utilizador` FOREIGN KEY (`utilizador`) REFERENCES `utilizador` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of utilizador_pagina
-- ----------------------------

-- ----------------------------
-- Table structure for utilizador_tipo
-- ----------------------------
DROP TABLE IF EXISTS `utilizador_tipo`;
CREATE TABLE `utilizador_tipo`  (
  `tipo` tinyint(1) NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`tipo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of utilizador_tipo
-- ----------------------------
INSERT INTO `utilizador_tipo` VALUES (0, 'Desativado');
INSERT INTO `utilizador_tipo` VALUES (1, 'Aluno');
INSERT INTO `utilizador_tipo` VALUES (2, 'Professor');
INSERT INTO `utilizador_tipo` VALUES (3, 'Coordenador');
INSERT INTO `utilizador_tipo` VALUES (4, 'Administrador');

SET FOREIGN_KEY_CHECKS = 1;
