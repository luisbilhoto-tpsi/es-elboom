<?PHP
  require_once 'includes/errorhandler.php';
  require_once 'includes/session.php';
  require_once 'includes/smarty.php';


  if (isset($_SESSION['USER_ID'])) {
    $smarty->display('disciplina.tpl');
  } else {
    ErrorHandler::throw('500');
  }
?>
