<?PHP
  require_once 'includes/database.php';
  require_once 'includes/errorhandler.php';
  require_once 'includes/filter.php';
  require_once 'includes/request.php';
  require_once 'includes/session.php';
  require_once 'includes/smarty.php';

  if (isset($_POST['submit']) && !isset($_SESSION['USER_ID'])) {
    $user = Filter::get('user');
    if (empty($user) || !preg_match('/^[a-zA-Z0-9@.-_\s]{1,64}$/', $user)) {
      $errors[] = '<b>Número ou e-mail</b> inválido!';
    }
    $password = Filter::get('password');
    if (empty($password) || !preg_match('/^[a-zA-Z0-9]{1,16}$/', $password)) {
      $errors[] = '<b>Palavra-passe</b> inválida!';
    }

    if (!isset($errors)) {
      $row = Database::queryRow(
        "SELECT u.id, u.nome, u.imagem, u.tipo, t.nome tnome FROM utilizador u INNER JOIN utilizador_tipo t ".
        "ON u.tipo = t.tipo WHERE email = '$user' AND password = PASSWORD('$password');"
      );
      if ($row > 0 && $row['tipo'] != 0) {
        $_SESSION['USER_ID']     = $row['id'];
        $_SESSION['USER_NAME']   = $row['nome'];
        $_SESSION['USER_AVATAR'] = $row['imagem'];
        $_SESSION['USER_TYPE']   = $row['tipo'];
        $_SESSION['USER_TNOME']  = $row['tnome'];
      } else {
        $smarty->assign('errors', [
          'O <b>número/e-mail</b> e/ou a <b>palavra-passe</b> estão incorretos.<br />'.
          'Por favor, tente novamente.'
        ]);
      }
    } else {
      $smarty->assign('errors', $errors);
    }
  }

  if (!isset($_SESSION['USER_ID']) || isset($_GET['action']) && $_GET['action'] === "sair") {
    $smarty->assign('fullscreen', true);
    $smarty->display('entrar.tpl');
    session_destroy();
  } else {
    switch ($_SESSION['USER_TYPE']) {
      case 1:
        $smarty->display('inicio_aluno.tpl');
        break;
      case 2 || 3:
        $smarty->display('inicio_aluno.tpl');
        break;
      case 4:
        $smarty->display('inicio_aluno.tpl');
        break;
      default:
        ErrorHandler::throw('500');
        break;
    }
  }
?>
