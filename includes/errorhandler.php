<?PHP
    require_once 'includes/smarty.php';

    class ErrorHandler {
        private static $list = array(
            '403' => [
                'error'   => '',
                'message' => '',
                'image'   => ''
            ],
            '404' => [
                'error'   => 'Página Não Encontrada',
                'message' => 'Não conseguimos encontrar a página que você está à procura.',
                'image'   => '404.png'
            ],
            '500' => [
                'error'   => 'Erro Interno do Servidor',
                'message' => 'Problemas desencadeados por permissões erradas de arquivos e diretórios.<br/>'.
                             'Reabra o navegador após limpar a cache e excluir os cookies.',
                'image'   => '500.png'
            ],
            'maintenance' => [
                'error'   => '',
                'message' => '',
                'image'   => ''
            ]
        );

        // Throws error
        public static function throw($code) {
            global $smarty;
            if (!array_key_exists($code, self::$list)) { $code = '404'; }
            $smarty->assign('page_title', $code.' '.self::$list[$code]['error']);
            $smarty->assign('error', self::$list[$code]['error']);
            $smarty->assign('message', self::$list[$code]['message']);
            $smarty->assign('image', self::$list[$code]['image']);
            $smarty->assign('fullscreen', true);
            $smarty->display('error.tpl');
            die();
        }
    }
?>