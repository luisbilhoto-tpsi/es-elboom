<?PHP
	class Filter {
		public static function get($var, $type='STRING', $method='POST') {
			$method = strtoupper($method);
			$type   = strtoupper($type);

			switch ($method) {
				case 'POST':
					$input = $_POST;
					break;
				case 'GET':
					$input = $_GET;
					break;
				case 'COOKIE':
					$input = $_COOKIE;
					break;
				case 'REQUEST':
					$input = $_REQUEST;
					break;
				case 'SERVER':
					$input = $_SERVER;
					break;
				default:
					trigger_error('Invalid filter method!', E_USER_ERROR);
			}

			$source = (isset($input[$var])) ? trim($input[$var]) : NULL;
			if (!$source) { return $source; }
			return Filter::xss($source);
		}

		public static function xss($content){
			require_once('vendors/htmlpurifier/HTMLPurifier.auto.php');
			$conf_Purifier = HTMLPurifier_Config::createDefault();
			$conf_Purifier->set('HTML.Allowed', 'p,b,strong,i');
			$sanitiser = new HTMLPurifier($conf_Purifier);
			$output = $sanitiser->purify($content);		
			return $output;
		}
	}
