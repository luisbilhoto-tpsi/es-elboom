<?PHP
	require_once 'configs/database.php';
	require_once 'includes/errorhandler.php';

    class Database {
        private static $connection;

        public static function connect($host, $user, $pass, $db) {
            self::$connection = mysqli_connect($host, $user, $pass, $db);
            mysqli_set_charset(self::$connection, 'utf8mb4');
            return self::$connection;
        }

        public static function close() {
            mysqli_close(self::$connection);
        }

        // Returns one row
        public static function queryRow($query) {
            $return = mysqli_query(self::$connection, $query);
            $return = mysqli_fetch_array($return, MYSQLI_ASSOC);
            return $return;
        }

        // Returns all rows
        public static function queryAll($query) {
            $return = mysqli_query(self::$connection, $query);
            $return = mysqli_fetch_all($return, MYSQLI_ASSOC);
            return $return;
        }

        // Returns the number of affected rows
        public static function query($query) {
            mysqli_query(self::$connection, $query);
            return mysqli_affected_rows(self::$connection);
        }
    }

	try {
		Database::connect(
			DATABASE['HOST'],
			DATABASE['USER'],
			DATABASE['PASS'],
			DATABASE['NAME']
		);
	} catch (PDOException $e) {
		ErrorHandler::throw('500');
	}
