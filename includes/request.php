<?PHP
	class Request {
		private static $path;
		private static $query;
		private static $request;
		private static $array;

		// Constructs class values
		public static function construct() {
			Request::$path    = str_replace('\\', '/', dirname(dirname(__FILE__)) . '\\');
			Request::$path    = str_replace($_SERVER['DOCUMENT_ROOT'], '', Request::$path);
			Request::$path    = Request::$path != '' ? Request::$path : '/';
			Request::$query   = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
			Request::$request = str_replace(Request::$path, '', $_SERVER['REQUEST_URI']);
			Request::$request = str_replace('?' . Request::$query, '', Request::$request);
			Request::$array   = explode('/', trim(Request::$request, '/'));
		}

		// Returns the request string
		public static function get() {
			return Request::$request;
		}

		// Returns the request array
		public static function array() {
			return Request::$array;
		}

		// Returns the value of a request argument
		public static function argument($search, $type = 'INT')
		{
			$arg   = NULL;
			$query = Request::array();
			foreach ($query as $key => $value) {
				if ($value == $search) {
					if (isset($query[$key + 1])) {
						$arg = $query[$key + 1];
					}
				}
			}
			return ($type == 'INT') ? intval($arg) : $arg;
		}

		// Returns the relative path
		public static function path() {
			return Request::$path;
		}

		// Returns the request query
		public static function query() {
			return Request::$query;
		}
	}

	// Initialize class
	Request::construct();
