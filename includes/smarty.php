<?PHP
	require_once 'includes/request.php';
	require_once 'vendors/smarty/Smarty.class.php';

	$smarty  = new Smarty();
	$smarty->compile_check = true;
	$smarty->debugging     = false;
	$smarty->compile_dir   = 'cache';
	$smarty->template_dir  = 'templates';
	$smarty->assign('base_url', Request::path());
