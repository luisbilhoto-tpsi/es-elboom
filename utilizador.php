<?PHP
  require_once 'includes/database.php';
  require_once 'includes/errorhandler.php';
  require_once 'includes/filter.php';
  require_once 'includes/request.php';
  require_once 'includes/session.php';
  require_once 'includes/smarty.php';

  if (isset($_SESSION['USER_ID'])) {
    if (isset(Request::array()[1]) && Request::array()[1] === 'editar') {
      if (isset($_GET['action']) && $_GET['action'] === "avatar_reset") {
        Database::query("UPDATE utilizador SET imagem = 'default.svg' WHERE id = '".$_SESSION['USER_ID']."';");
        $_SESSION['USER_AVATAR'] = 'default.svg';
      }

      if (isset($_POST['utilizador'])) {
        if (isset($_FILES['avatar']['name']) && !$_FILES['avatar']['error']) {
          $extension = strtolower(pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION));
          if (!in_array($extension, ["jpg", "jpeg", "png"])) {
            $errors[] = '<b>Extensão da imagem</b> inválida! (jpg/png)';
          } elseif ($_FILES['avatar']['size'] > (1024000)) {
            $errors[] = '<b>Tamanho da imagem</b> inválido! (Máx: 1Mb)';
          } else {
            $image = $_SESSION['USER_ID'] . ".$extension";
            move_uploaded_file($_FILES['avatar']['tmp_name'], "uploads/avatars/$image");
            Database::query("UPDATE utilizador SET imagem = '$image' WHERE id = '".$_SESSION['USER_ID']."';");
            $_SESSION['USER_AVATAR'] = $image;
          }
        }

        $name = Filter::get('nome');
        if (empty($name) || !preg_match('/^[a-zA-Z0-9\s]{1,32}$/', $name)) {
          $errors[] = '<b>Nome completo</b> inválido!';
        }
        $email = Filter::get('email');
        if (empty($email) || !preg_match('/^[a-zA-Z0-9@.-_]{1,64}$/', $email)) {
          $errors[] = '<b>Endereço de e-mail</b> inválido!';
        }

        if (!isset($errors)) {
          if(Database::query("UPDATE utilizador SET nome = '$name', email = '$email' WHERE id = '".$_SESSION['USER_ID']."';")) {
            $_SESSION['USER_NAME']  = $name;
          }
        }
      }

      if (isset($_POST['seguranca'])) {
        $password1 = Filter::get('password1');
        if (empty($password1) || !preg_match('/^[a-zA-Z0-9]{1,16}$/', $password1)) {
          $errors[] = '<b>Nova Palavra-passe</b> inválida!';
        }
        $password2 = Filter::get('password2');
        if (empty($password2) || !preg_match('/^[a-zA-Z0-9]{1,16}$/', $password2)) {
          $errors[] = '<b>Palavra-passe de confirmação</b> inválida!';
        }
        if ($password1 !== $password2) {
          $errors[] = 'As palavras-passe não correspondem!';
        }

        if (!isset($errors)) {
          Database::query("UPDATE utilizador SET password = PASSWORD('$password1') WHERE id = '".$_SESSION['USER_ID']."';");
          header('Location: ../inicio?action=sair');
        }
      }

      $smarty->assign('user', Database::queryRow("SELECT * FROM utilizador WHERE id = '".$_SESSION['USER_ID']."';"));
      if (isset($errors)) { $smarty->assign('errors', $errors); }
      $smarty->display('utilizador_editar.tpl');
    } else {
      ErrorHandler::throw('404');
    }
  } else {
    ErrorHandler::throw('500');
  }
?>
