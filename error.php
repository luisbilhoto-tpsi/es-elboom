<?PHP
    require_once 'includes/request.php';
    require_once 'includes/errorhandler.php';

    $code = Request::argument('error', 'STR');
    $code = $code != '' ? $code : Request::query();
    ErrorHandler::throw($code);
?>